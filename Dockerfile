FROM mcr.microsoft.com/dotnet/sdk:5.0.100-alpine3.12 AS build
WORKDIR /app

COPY NuGet.config ./
COPY *.sln .
COPY src/Presentation.API/Presentation.API.csproj ./src/Presentation.API/
COPY src/Domain.Core/Domain.Core.csproj ./src/Domain.Core/
COPY src/Domain.Entities/Domain.Entities.csproj ./src/Domain.Entities/
COPY src/Infrastructure/Infrastructure.csproj ./src/Infrastructure/

COPY test/Domain.Core.Test/Domain.Core.Test.csproj ./test/Domain.Core.Test/

RUN dotnet restore

COPY . ./

RUN dotnet publish /app/src/Presentation.API -c Release -o /app/src/Presentation.API/out

FROM mcr.microsoft.com/dotnet/aspnet:5.0.0-alpine3.12 AS runtime
WORKDIR /app

RUN apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT false

COPY --from=build /app/src/Presentation.API/out ./
ENTRYPOINT ["dotnet", "Presentation.API.dll"]