﻿using Domain.Core.Services;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Domain.Core
{
    public static class ServiceExtensions
    {
        public static void AddDomainCoreLayer(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddScoped(typeof(IOrderService), typeof(OrderService));
        }
    }
}
