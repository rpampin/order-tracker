﻿using Domain.Core.Commons.DTO;
using Domain.Core.Exceptions;
using Domain.Core.ExternalService;
using Domain.Core.Repositories;
using FVG.Utils.Domain.Exceptions;
using FVG.Utils.Logging.Abstractions;
using System;
using System.Linq;
using System.Threading.Tasks;
using ENT = Domain.Entities;

namespace Domain.Core.Services
{
    public interface IOrderService
    {
        Task CreateAsync(Order order);
        Task CreatePaymentEventAsync(PaymentEvent paymentEvent);
        Task CreateDeliveryEventAsync(DeliveryEvent deliveryEvent);
        Task CreateBillingEventAsync(BillingEvent billingEvent);
        Task CreateCancelEventAsync(Event cancelEvent);
    }

    public class OrderService : IOrderService
    {
        private readonly IBillingService _billingService;
        private readonly IDeliveryManagerService _deliveryManagerService;
        private readonly IOrderRepository _orderRepository;
        private readonly ILogger<OrderService> _logger;

        public OrderService(
            IBillingService billingService,
            IDeliveryManagerService deliveryManagerService,
            IOrderRepository orderRepository,
            ILogger<OrderService> logger)
        {
            _billingService = billingService;
            _deliveryManagerService = deliveryManagerService;
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task CreateAsync(Order order)
        {
            _logger.LogInformation("receiving order", new { order });

            var newOrder = order.ToEntity();

            try
            {
                _logger.LogDebug("Saving");
                await _orderRepository.AddAsync(newOrder);
                _logger.LogDebug("Order saved");
            }
            catch (DuplicateEntityException<Entities.Order> ex)
            {
                _logger.LogDebug($"The order is duplicated", new { oldOrder = ex.OldEntity, newOrder });
                throw new UserException($"Order with id '{order.OrderId}' already exists in '{ex.OldEntity.CreatedOn}'");
            }
        }

        public async Task CreatePaymentEventAsync(PaymentEvent paymentEvent)
        {
            _logger.LogInformation("receiving order payment event", new { paymentEvent });

            _logger.LogDebug("Loading and updating order");
            var order = await _orderRepository.GetAsync(paymentEvent.OrderId);
            order.AddEvent(new(ENT.EventTypeEnum.PaymentReceived));
            order.AddPayment(new(paymentEvent.Amount, paymentEvent.ApporvedOn));

            if (order.Payments.Sum(p => p.Amount) >= order.Amount)
            {
                order.Payed();
                await _billingService.SendOrderBillingEvent(order.OrderId, order.Amount);
                await _deliveryManagerService.SendOrderDeliveryEvent(order.OrderId, order.DeliveryAddress);
            }

            _logger.LogDebug("Saving");
            await _orderRepository.UpdateAsync(order);
            _logger.LogDebug("Order saved");
        }

        public async Task CreateBillingEventAsync(BillingEvent billingEvent)
        {
            _logger.LogInformation("receiving order billing event", new { billingEvent });

            _logger.LogDebug("Loading and updating order");
            var order = await _orderRepository.GetAsync(billingEvent.OrderId);

            order.AddEvent(new(ENT.EventTypeEnum.Billing));
            order.Billed(billingEvent.InvoiceNumber);

            _logger.LogDebug("Saving");
            await _orderRepository.UpdateAsync(order);
            _logger.LogDebug("Order saved");
        }

        public async Task CreateDeliveryEventAsync(DeliveryEvent deliveryEvent)
        {
            _logger.LogInformation("receiving order delivery event", new { deliveryEvent });

            _logger.LogDebug("Loading and updating order");
            var order = await _orderRepository.GetAsync(deliveryEvent.OrderId);
                        
            order.AddEvent(new(ENT.EventTypeEnum.Delivery));
            order.Delivered(deliveryEvent.DeliveryOn);

            _logger.LogDebug("Saving");
            await _orderRepository.UpdateAsync(order);
            _logger.LogDebug("Order saved");
        }

        public async Task CreateCancelEventAsync(Event cancelEvent)
        {
            _logger.LogInformation("receiving order cancellation event", new { cancelEvent });

            _logger.LogDebug("Loading and updating order");
            var order = await _orderRepository.GetAsync(cancelEvent.OrderId);

            order.AddEvent(new(ENT.EventTypeEnum.Cancel));
            order.Cancel();

            _logger.LogDebug("Saving");
            await _orderRepository.UpdateAsync(order);
            _logger.LogDebug("Order saved");
        }
    }
}
