﻿using Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Domain.Core.ExternalService
{
    public interface IDeliveryManagerService
    {
        Task SendOrderDeliveryEvent(Guid orderId, DeliveryAddress deliveryAddress);
    }
}
