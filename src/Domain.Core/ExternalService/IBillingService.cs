﻿using System;
using System.Threading.Tasks;

namespace Domain.Core.ExternalService
{
    public interface IBillingService
    {
        Task SendOrderBillingEvent(Guid orderId, decimal orderAmount);
    }
}
