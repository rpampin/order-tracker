﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Core.Repositories
{
    public interface IOrderRepository
    {
        Task AddAsync(Order order);
        Task<Order> GetAsync(Guid orderId);
        Task<List<Order>> GetAllAsync();
        Task UpdateAsync(Order order);
    }
}
