﻿using Domain.Core.Commons.Mapping;
using FluentValidation;

namespace Domain.Core.Commons.DTO
{
    public class Product : IEntitiable<Entities.Product>
    {
        public string SKU { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }

        public Entities.Product ToEntity() => new(SKU, Quantity, Price, Total);

        public class ProductValidator : AbstractValidator<Product>
        {
            public ProductValidator()
            {
                RuleFor(x => x.SKU).NotEmpty();
                RuleFor(x => x.Quantity).NotEmpty();
                RuleFor(x => x.Price).NotEmpty();
                RuleFor(x => x.Total).NotEmpty();
            }
        }
    }
}