﻿using Domain.Core.Commons.Mapping;
using System;

namespace Domain.Core.Commons.DTO
{
    public class DeliveryAddress : Address, IEntitiable<Entities.DeliveryAddress>
    { 
        public GeoCoordinates GeoCoordinates {get; set;}
        public bool UseToBilling { get; set; }

        public new Entities.DeliveryAddress ToEntity() => new Entities.DeliveryAddress(Street, Number, GeoCoordinates.ToEntity());
    }
}