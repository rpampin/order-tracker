﻿using System;

namespace Domain.Core.Commons.DTO
{
    public class DeliveryEvent : Event
    {
        public DateTime DeliveryOn { get; set; }
    }
}
