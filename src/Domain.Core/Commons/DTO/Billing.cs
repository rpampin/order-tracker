﻿using Domain.Core.Commons.Mapping;
using FluentValidation;

namespace Domain.Core.Commons.DTO
{
    public class Billing : IEntitiable<Entities.Billing>
    {
        public Payer Payer { get; set; }

        public Entities.Billing ToEntity() => new Entities.Billing(Payer.ToEntity());

        public class BillingValidator : AbstractValidator<Billing>
        {
            public BillingValidator()
            {
                RuleFor(x => x.Payer).NotEmpty();
            }
        }
    }
}