﻿using Domain.Core.Commons.Mapping;
using FluentValidation;

namespace Domain.Core.Commons.DTO
{
    public class GeoCoordinates : IEntitiable<Entities.GeoCoordinates>
    {
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }

        public Entities.GeoCoordinates ToEntity() => new Entities.GeoCoordinates(Latitud, Longitud);

        public class GeoCoordinatesValidator : AbstractValidator<GeoCoordinates>
        {
            public GeoCoordinatesValidator()
            {
                RuleFor(x => x.Latitud).NotEmpty();
                RuleFor(x => x.Longitud).NotEmpty();
            }
        }
    }
}