﻿using Domain.Core.Commons.Mapping;
using FluentValidation;

namespace Domain.Core.Commons.DTO
{
    public class Identification : IEntitiable<Entities.Identification>
    {
        public string Type { get; set;  }
        public string Number { get; set; }

        public Entities.Identification ToEntity() => new Entities.Identification(Type, Number);

        public class IdentificationValidator : AbstractValidator<Identification>
        {
            public IdentificationValidator()
            {
                RuleFor(x => x.Type).NotEmpty();
                RuleFor(x => x.Number).NotEmpty();
            }
        }
    }
}