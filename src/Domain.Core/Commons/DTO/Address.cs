﻿using Domain.Core.Commons.Mapping;
using FluentValidation;

namespace Domain.Core.Commons.DTO
{
    public class Address : IEntitiable<Entities.Address>
    {
        public string Street { get; set;  }
        public string Number { get; set; }

        public Entities.Address ToEntity() => new Entities.Address(Street, Number);

        public class AddressValidator : AbstractValidator<Address>
        {
            public AddressValidator()
            {
                RuleFor(x => x.Street).NotEmpty();
                RuleFor(x => x.Number).NotEmpty();
            }
        }
    }
}