﻿using Domain.Core.Commons.Mapping;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Core.Commons.DTO
{
    public class Order : IEntitiable<Entities.Order>
    {
        public Guid OrderId { get; set; }
        public Guid Cuid { get; set; }
        public DateTime PurchaseOn { get; set; }
        public decimal Amount { get; set; }
        public DeliveryAddress DeliveryAddress { get; set; }
        public Billing Billing { get; set; }
        public Address BillingAddress { get; set; }
        public IEnumerable<Product> Products { get; set; }

        public Entities.Order ToEntity() => new Entities.Order(OrderId, Cuid, PurchaseOn, Amount, DeliveryAddress.ToEntity(), Billing.ToEntity(), BillingAddress?.ToEntity(), Products.ToEntity());
    }
}
