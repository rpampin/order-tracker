﻿using Domain.Core.Commons.Mapping;
using FluentValidation;

namespace Domain.Core.Commons.DTO
{
    public class Payer : IEntitiable<Entities.Payer>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Identification Identification { get; set; }

        public Entities.Payer ToEntity() => new Entities.Payer(FirstName, LastName, Identification.ToEntity());

        public class PayerValidator : AbstractValidator<Payer>
        {
            public PayerValidator()
            {
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.Identification).NotEmpty();
            }
        }
    }
}