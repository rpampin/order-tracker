﻿using System;

namespace Domain.Core.Commons.DTO
{
    public class PaymentEvent : Event
    {
        public decimal Amount { get; set; }
        public DateTime ApporvedOn { get; set; }
    }
}
