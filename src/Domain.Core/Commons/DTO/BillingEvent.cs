﻿namespace Domain.Core.Commons.DTO
{
    public class BillingEvent : Event
    {
        public string InvoiceNumber { get; set; }
    }
}
