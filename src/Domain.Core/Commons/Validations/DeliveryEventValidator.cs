﻿using Domain.Core.Commons.DTO;
using FluentValidation;

namespace Domain.Core.Commons.Validations
{
    public class DeliveryEventValidator : AbstractValidator<DeliveryEvent>
    {
        public DeliveryEventValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
            RuleFor(x => x.DeliveryOn).NotEmpty();
        }
    }
}
