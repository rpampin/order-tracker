﻿using Domain.Core.Commons.DTO;
using FluentValidation;

namespace Domain.Core.Commons.Validations
{
    public class EventValidator : AbstractValidator<Event>
    {
        public EventValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
        }
    }
}
