﻿using Domain.Core.Commons.DTO;
using FluentValidation;

namespace Domain.Core.Commons.Validations
{
    public class PaymentEventValidator : AbstractValidator<PaymentEvent>
    {
        public PaymentEventValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
            RuleFor(x => x.ApporvedOn).NotEmpty();
            RuleFor(x => x.Amount).NotEmpty().Must(x => x > 0);
        }
    }
}
