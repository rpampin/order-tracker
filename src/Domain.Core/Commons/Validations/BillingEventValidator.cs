﻿using Domain.Core.Commons.DTO;
using FluentValidation;

namespace Domain.Core.Commons.Validations
{
    public class BillingEventValidator : AbstractValidator<BillingEvent>
    {
        public BillingEventValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
            RuleFor(x => x.InvoiceNumber).NotEmpty();
        }
    }
}
