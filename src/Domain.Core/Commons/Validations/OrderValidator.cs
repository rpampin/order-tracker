﻿using Domain.Core.Commons.DTO;
using FluentValidation;

namespace Domain.Core.Commons.Validations
{
    public class OrderValidator : AbstractValidator<Order>
    {
        public OrderValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
            RuleFor(x => x.Cuid).NotEmpty();
            RuleFor(x => x.PurchaseOn).NotEmpty();
            RuleFor(x => x.Amount).NotEmpty().Must(a => a > 0);
            RuleFor(x => x.Billing).NotEmpty();

            RuleFor(x => x.DeliveryAddress).NotNull().DependentRules(() =>
            {
                RuleFor(x => x.BillingAddress).Null().When(x => x.DeliveryAddress.UseToBilling);
                RuleFor(x => x.BillingAddress).NotNull().When(x => !x.DeliveryAddress.UseToBilling);
            });

            RuleFor(x => x.Products).NotEmpty();
        }
    }
}