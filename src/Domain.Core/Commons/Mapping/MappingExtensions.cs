﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Core.Commons.Mapping
{
    public static class MappingExtensions
    {
        public static IEnumerable<Entities.Product> ToEntity(this IEnumerable<DTO.Product> products)
            => products is not null
                ? products.Select(product => product.ToEntity())
                : Enumerable.Empty<Entities.Product>();
    }
}
