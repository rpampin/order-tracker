﻿namespace Domain.Core.Commons.Mapping
{
    public interface IEntitiable<TEntity>
    {
        TEntity ToEntity();
    }
}
