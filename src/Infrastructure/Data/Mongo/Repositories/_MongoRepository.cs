﻿using Infrastructure.Data.Mongo.Options;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;

namespace Infrastructure.Data.Mongo.Repositories
{
    public abstract class MongoRepository<T>
    {
        protected readonly IMongoDatabase _db;

        public MongoRepository(IMongoClient client, IOptionsSnapshot<MongoOptions> options)
        {
            if (client is null)
                throw new ArgumentNullException(nameof(client));

            if (options is null)
                throw new ArgumentNullException(nameof(options));

            _db = client.GetDatabase(options.Value.Database);
        }

        protected abstract string MongoCollectionName { get; }

        protected IMongoCollection<T> Collection => _db.GetCollection<T>(MongoCollectionName);

        protected IMongoQueryable<T> CollectionQuery => Collection.AsQueryable();
    }
}
