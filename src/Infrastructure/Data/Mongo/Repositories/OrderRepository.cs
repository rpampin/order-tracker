﻿using Domain.Core.Exceptions;
using Domain.Core.Repositories;
using Domain.Entities;
using FVG.Utils.Domain.Exceptions;
using Infrastructure.Data.Mongo.Options;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Data.Mongo.Repositories
{
    public class OrderRepository : MongoRepository<Order>, IOrderRepository
    {
        public OrderRepository(IMongoClient client, IOptionsSnapshot<MongoOptions> options) : base(client, options)
        {
        }

        public static readonly string CollectionName = "orders";
        public static readonly string DocumentUniqueConstraint = "order_unique_constraint";
        protected override string MongoCollectionName => CollectionName;

        public async Task AddAsync(Order order)
        {
            try
            {
                await Collection.InsertOneAsync(order);
            }
            catch (MongoWriteException ex) when (ex.WriteError.Category == ServerErrorCategory.DuplicateKey)
            {
                throw new DuplicateEntityException<Order>(order);
            }
        }

        public Task<Order> GetAsync(Guid orderId)
        {
            return CollectionQuery
                .FirstOrDefaultAsync(x => x.OrderId == orderId)
                ?? throw new EntityNotFoundException<Order>(orderId);
        }

        public Task<List<Order>> GetAllAsync()
        {
            return CollectionQuery.ToListAsync();
        }

        public async Task UpdateAsync(Order order)
        {
            await Collection.ReplaceOneAsync(o => o.OrderId == order.OrderId, order);
        }
    }
}
