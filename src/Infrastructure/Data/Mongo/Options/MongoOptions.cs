﻿namespace Infrastructure.Data.Mongo.Options
{
    public class MongoOptions
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public string TimeoutInSeconds { get; set; }
    }
}
