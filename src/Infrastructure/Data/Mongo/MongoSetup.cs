﻿using Domain.Entities;
using Infrastructure.Data.Mongo.Options;
using Infrastructure.Data.Mongo.Repositories;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace Infrastructure.Data.Mongo
{
    public class MongoSetup
    {
        private readonly IMongoDatabase _db;

        public MongoSetup(IMongoClient client, IOptionsSnapshot<MongoOptions> options)
        {
            if (client is null)
                throw new System.ArgumentNullException(nameof(client));

            if (options is null)
                throw new System.ArgumentNullException(nameof(options));

            _db = client.GetDatabase(options.Value.Database);
        }

        public void CreateIndexes()
        {
            //TODO: NECESITO CREAR INDICES ??
            
            //var documentUniqueConstraint = new CreateIndexModel<Order>(
            //    Builders<Order>.IndexKeys
            //        .Ascending(o => o.Cuid)
            //    new CreateIndexOptions<Order>
            //    {
            //        Unique = true,
            //        Name = OrderRepository.DocumentUniqueConstraint
            //    });

            //var collection = _db.GetCollection<Order>(OrderRepository.CollectionName);
            //collection.Indexes.CreateMany(new[] { documentUniqueConstraint });
        }

        public static void OnStartup()
        {
            //BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard));
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;

            ConventionRegistry.Register("OrderConventions",
                new ConventionPack {
                    new IgnoreExtraElementsConvention(true),
                    new CamelCaseElementNameConvention(),
                    new EnumRepresentationConvention(BsonType.String)
                },
                _ => true);

            BsonClassMap.RegisterClassMap<Order>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(p => p.OrderId);
            });
        }
    }
}
