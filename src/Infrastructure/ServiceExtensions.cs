﻿using Domain.Core.ExternalService;
using Domain.Core.Repositories;
using Infrastructure.Data.Mongo;
using Infrastructure.Data.Mongo.Options;
using Infrastructure.Data.Mongo.Repositories;
using Infrastructure.Mocks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Infrastructure
{
    public static class ServiceExtensions
    {
        public static void AddInfraestructureLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRestClient();
            services.AddOptions<MongoOptions>();
            services.Configure<MongoOptions>(configuration.GetSection(nameof(MongoOptions)));
            services.AddScoped<IMongoClient>(sp => new MongoClient(sp.GetRequiredService<IOptionsSnapshot<MongoOptions>>().Value.ConnectionString));
            services.AddScoped(sp => new MongoSetup(
                new MongoClient(sp.GetRequiredService<IOptionsSnapshot<MongoOptions>>().Value.ConnectionString),
                sp.GetRequiredService<IOptionsSnapshot<MongoOptions>>()));
            services.AddScoped(typeof(IOrderRepository), typeof(OrderRepository));
            //MOCKS
            services.AddScoped(typeof(IBillingService), typeof(BillingPublished));
            services.AddScoped(typeof(IDeliveryManagerService), typeof(DeliveryManagerPublished));
        }
    }
}
