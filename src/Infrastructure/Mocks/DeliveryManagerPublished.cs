﻿using Domain.Core.ExternalService;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Mocks
{
    public class DeliveryManagerPublished : IDeliveryManagerService
    {
        public Task SendOrderDeliveryEvent(Guid orderId, Domain.Entities.DeliveryAddress deliveryAddress)
        {
            return Task.CompletedTask;
        }
    }
}
