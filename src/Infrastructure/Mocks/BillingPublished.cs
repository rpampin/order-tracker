﻿using Domain.Core.ExternalService;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Mocks
{
    public class BillingPublished : IBillingService
    {
        public Task SendOrderBillingEvent(Guid orderId, decimal orderAmount)
        {
            return Task.CompletedTask;
        }
    }
}
