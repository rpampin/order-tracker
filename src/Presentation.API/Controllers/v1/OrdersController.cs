﻿using Domain.Core.Repositories;
using Domain.Core.Services;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTO = Domain.Core.Commons.DTO;

namespace Presentation.API.Controllers.v1
{
    [ApiController]
    [Route("orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IOrderRepository _orderRepository;

        public OrdersController(
            IOrderService orderService,
            IOrderRepository orderRepository)
        {
            _orderService = orderService ?? throw new ArgumentNullException(nameof(orderService));
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        }

        [HttpGet("{orderId}")]
        public async Task<Order> GetOrder(Guid orderId)
        {
            return await _orderRepository.GetAsync(orderId);
        }

        [HttpGet]
        public async Task<List<Order>> GetAllOrders()
        {
            return await _orderRepository.GetAllAsync();
        }

        [HttpPost]
        public async Task AddOrder([FromBody] DTO.Order order)
        {
            await _orderService.CreateAsync(order);
        }
    }
}
