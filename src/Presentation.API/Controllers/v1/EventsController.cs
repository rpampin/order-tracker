﻿using Domain.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using DTO = Domain.Core.Commons.DTO;

namespace Presentation.API.Controllers.v1
{
    [ApiController]
    [Route("events")]
    public class EventsController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public EventsController(IOrderService orderService)
        {
            _orderService = orderService ?? throw new ArgumentNullException(nameof(orderService));
        }

        [HttpPost("payment")]
        public async Task AddOrderPayment([FromBody] DTO.PaymentEvent paymentEvent)
        {
            await _orderService.CreatePaymentEventAsync(paymentEvent);
        }

        [HttpPost("delivery")]
        public async Task AddOrderDelivery([FromBody] DTO.DeliveryEvent deliveryEvent)
        {
            await _orderService.CreateDeliveryEventAsync(deliveryEvent);
        }

        [HttpPost("billing")]
        public async Task AddOrderBilling([FromBody] DTO.BillingEvent billingEvent)
        {
            await _orderService.CreateBillingEventAsync(billingEvent);
        }

        [HttpPost("cancel")]
        public async Task CancelOrder([FromBody] DTO.Event cancelEvent)
        {
            await _orderService.CreateCancelEventAsync(cancelEvent);
        }
    }
}
