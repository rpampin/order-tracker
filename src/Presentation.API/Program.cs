using Infrastructure.Data.Mongo;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Web;
using System;
using System.Threading.Tasks;
using Logger = FVG.Utils.Logging.Abstractions;

namespace Presentation.API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            using var scope = host.Services.CreateScope();
            var logger = scope.ServiceProvider.GetRequiredService<Logger.ILogger<Program>>();
            try
            {
                MongoSetup.OnStartup();
                var mongoSetup = scope.ServiceProvider.GetRequiredService<MongoSetup>();
                mongoSetup.CreateIndexes();
                await host.RunAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error ocurred when running the host");
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging((context, logging) =>
                {
                    logging.ClearProviders();
                    logging
                        .AddConfiguration(context.Configuration.ReplacePlaceholders()
                        .GetSection("Logging"));
                }).UseNLog()
                .UseDefaultServiceProvider((context, options) =>
                {
                    options.ValidateScopes = false;
                    options.ValidateOnBuild = true;
                });
    }
}
