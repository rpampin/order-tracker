﻿using System;

namespace Domain.Entities
{
    public class Payment
    {
        public Payment(decimal amount, DateTime appovedOn)
        {
            Amount = amount;
            AppovedOn = appovedOn;
        }

        public decimal Amount { get; private set; }
        public DateTime AppovedOn { get; private set; }
    }
}