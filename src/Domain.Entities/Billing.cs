﻿using System;

namespace Domain.Entities
{
    public class Billing
    {
        public Billing(Payer payer)
        {
            Payer = payer ?? throw new ArgumentNullException(nameof(payer));
        }

        public Payer Payer { get; private set; }
        public string InvoiceNumber { get; private set; }

        public void AddInvoiceNumber(string invoiceNumber) => InvoiceNumber = invoiceNumber;
    }
}