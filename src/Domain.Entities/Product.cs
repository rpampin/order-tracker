﻿using System;

namespace Domain.Entities
{
    public class Product
    {
        public Product(string sku, int quantity, decimal price, decimal total)
        {
            SKU = sku ?? throw new ArgumentNullException(nameof(sku));
            Quantity = quantity;
            Price = price;
            Total = total;
        }

        public string SKU { get; private set; }
        public int Quantity { get; private set; }
        public decimal Price { get; private set; }
        public decimal Total { get; private set; }
    }
}