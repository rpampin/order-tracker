﻿namespace Domain.Entities
{
    public enum EventTypeEnum
    {
        PaymentReceived,
        Delivery,
        Billing,
        Cancel
    }
}
