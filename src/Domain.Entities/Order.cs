﻿using Domain.Entities.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Domain.Entities
{
    public class Order
    {
        public Guid OrderId { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public DateTime? UpdatedOn { get; private set; }
        public Guid Cuid { get; private set; }
        public DateTime PurchaseOn { get; private set; }
        public decimal Amount { get; private set; }
        public DeliveryAddress DeliveryAddress { get; private set; }
        public Billing Billing { get; private set; }
        public Address BillingAddress { get; private set; }
        public Status CurrentStatus { get; private set; }
        public IEnumerable<Product> Products { get; private set; }
        public ICollection<Payment> Payments { get; private set; }
        public ICollection<Event> Events { get; private set; }
        public ICollection<StatusTrantision> Transitions { get; private set; }

        public Order(Guid orderId, Guid cuid, DateTime purchaseOn, decimal amount, DeliveryAddress deliveryAddress, Billing billing, Address billingAddress, IEnumerable<Product> products)
        {
            OrderId = orderId;
            Cuid = cuid;
            PurchaseOn = purchaseOn;
            Amount = amount;
            DeliveryAddress = deliveryAddress;
            Billing = billing;
            BillingAddress = billingAddress;
            Products = products;
            CreatedOn = DateTime.UtcNow;
            CurrentStatus = new Status();

            Events = new Collection<Event>();
            Payments = new Collection<Payment>();
            Transitions = new Collection<StatusTrantision> { new(CurrentStatus.Value) };
        }

        void Update() => UpdatedOn = DateTime.UtcNow;
        bool IsCreated() => CurrentStatus.IsCreated();
        void RegisterStatusTransition(Status currentStatus) => Transitions.Add(new(currentStatus.Value));

        public void AddEvent(Event @event)
        {
            if (CurrentStatus.IsEnding())
                throw new OrderEndedException(CurrentStatus.Value.ToString());
            Update();
            Events.Add(@event);
        }

        public void AddPayment(Payment payment)
        {
            Payments.Add(payment);
            if (IsCreated())
            {
                CurrentStatus.PaymentReceived();
                RegisterStatusTransition(CurrentStatus);
            }
        }

        public void Payed()
        {
            if (CurrentStatus.IsPending())
            {
                CurrentStatus.Payed();
                RegisterStatusTransition(CurrentStatus);
            }
        }

        public void Delivered(DateTime deliveryOn)
        {
            if (!CurrentStatus.CanBeDeliveredOrBilled())
                throw new StatusChangeNotAllowedException(CurrentStatus.Value.ToString(), Status.Options.Delivered.ToString());
            CurrentStatus.Delivered();
            DeliveryAddress.DeliveredOn(deliveryOn);
            RegisterStatusTransition(CurrentStatus);
            if (CurrentStatus.CanBeEnded(Transitions.Select(t => t.Value).ToArray()))
            {
                CurrentStatus.Terminate();
                RegisterStatusTransition(CurrentStatus);
            }
        }

        public void Billed(string invoiceNumber)
        {
            if (!CurrentStatus.CanBeDeliveredOrBilled())
                throw new StatusChangeNotAllowedException(CurrentStatus.Value.ToString(), Status.Options.Billing.ToString());
            CurrentStatus.Billed();
            Billing.AddInvoiceNumber(invoiceNumber);
            RegisterStatusTransition(CurrentStatus);
            if (CurrentStatus.CanBeEnded(Transitions.Select(t => t.Value).ToArray()))
            {
                CurrentStatus.Terminate();
                RegisterStatusTransition(CurrentStatus);
            }
        }

        public void Cancel()
        {
            CurrentStatus.Cancel();
            RegisterStatusTransition(CurrentStatus);
        }
    }
}
