﻿using System;

namespace Domain.Entities
{
    public class DeliveryAddress : Address
    {
        public DeliveryAddress(string street, string number, GeoCoordinates geoCoordinates) 
            : base(street, number)
        {
            GeoCoordinates = geoCoordinates;
        }

        public GeoCoordinates GeoCoordinates {get; private set;}
        public DateTime? DeliveryOn { get; private set; }

        public void DeliveredOn(DateTime deliveryOn) => DeliveryOn = deliveryOn;
    }
}