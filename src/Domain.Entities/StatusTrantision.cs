﻿using System;

namespace Domain.Entities
{
    public class StatusTrantision : Status
    {
        public StatusTrantision(Options value)
            : base(value)
        {
            CreatedOn = DateTime.UtcNow;
        }

        public DateTime CreatedOn { get; private set; }
    }
}
