﻿using FVG.Utils.Domain.Exceptions;

namespace Domain.Entities.Exceptions
{
    public class StatusChangeNotAllowedException : UserException
    {
        public StatusChangeNotAllowedException(string actual, string notAllowed)
            : base($"Status change not allowed, '{actual}' to '{notAllowed}'")
        { }
    }
}
