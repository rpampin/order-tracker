﻿using FVG.Utils.Domain.Exceptions;

namespace Domain.Entities.Exceptions
{
    public class OrderEndedException : UserException
    {
        public OrderEndedException(string actual)
            : base($"Order cannot be changed because of final state '{actual}'")
        { }
    }
}
