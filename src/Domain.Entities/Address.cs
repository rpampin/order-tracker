﻿using System;

namespace Domain.Entities
{
    public class Address
    {
        public Address(string street, string number)
        {
            Street = street ?? throw new ArgumentNullException(nameof(street));
            Number = number ?? throw new ArgumentNullException(nameof(number));
        }

        public string Street { get; private set; }
        public string Number { get; private set; }
    }
}