﻿using System;

namespace Domain.Entities
{
    public class Event
    {
        public Event(EventTypeEnum type)
        {
            Type = type;
            EventOn = DateTime.UtcNow;
        }

        public EventTypeEnum Type { get; private set; }
        public DateTime EventOn { get; private set; }
    }
}