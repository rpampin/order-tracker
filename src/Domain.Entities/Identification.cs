﻿using System;

namespace Domain.Entities
{
    public class Identification
    {
        public Identification(string type, string number)
        {
            Type = type ?? throw new ArgumentNullException(nameof(type));
            Number = number ?? throw new ArgumentNullException(nameof(number));
        }

        public string Type { get; private set;  }
        public string Number { get; private set; }
    }
}