﻿using System;

namespace Domain.Entities
{
    public class Payer
    {
        public Payer(string firstName, string lastName, Identification identification)
        {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
            Identification = identification ?? throw new ArgumentNullException(nameof(identification));
        }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public Identification Identification { get; private set; }
    }
}