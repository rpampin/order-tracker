﻿using System;
using System.Linq;

namespace Domain.Entities
{
    public class Status
    {
        public Status(Options value = Options.Created)
        {
            Value = value;
        }

        public enum Options
        {
            Created = 1,
            PaymentReceived,
            Payed,
            Delivered,
            Billing,
            Terminated,
            Cancelled
        }

        public static Options[] PendingStates =
        {
            Options.Created,
            Options.PaymentReceived
        };

        public static Options[] DeliveredOrBilledSourceStates =
        {
            Options.Payed,
            Options.Billing,
            Options.Delivered
        };

        public static Options[] EndingStates =
        {
            Options.Cancelled,
            Options.Terminated
        };

        public Options Value { get; private set; }

        public bool IsCreated() => Value == Options.Created;
        public bool IsPending() => PendingStates.Contains(Value);
        public bool IsEnding() => EndingStates.Contains(Value);
        public bool CanBeDeliveredOrBilled() => DeliveredOrBilledSourceStates.Contains(Value);
        public bool CanBeEnded(Options[] transitions) => DeliveredOrBilledSourceStates.All(i => transitions.Contains(i));
        public void PaymentReceived() => Value = Options.PaymentReceived;
        public void Payed() => Value = Options.Payed;
        public void Delivered() => Value = Options.Delivered;
        public void Billed() => Value = Options.Billing;
        public void Terminate() => Value = Options.Terminated;
        public void Cancel() => Value = Options.Cancelled;

    }
}