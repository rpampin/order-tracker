﻿namespace Domain.Entities
{
    public class GeoCoordinates
    {
        public GeoCoordinates(decimal latitud, decimal longitud)
        {
            Latitud = latitud;
            Longitud = longitud;
        }

        public decimal Latitud { get; private set; }
        public decimal Longitud { get; private set; }
    }
}