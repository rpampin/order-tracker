﻿using AutoFixture.Xunit2;
using Domain.Core.Exceptions;
using Domain.Core.Repositories;
using Domain.Core.Services;
using FluentAssertions;
using FVG.Utils.Domain.Exceptions;
using FVG.Utils.Testing.Fixtures.Attributes;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using CDTO = Domain.Core.Commons.DTO;
using ENT = Domain.Entities;

namespace Domain.Core.Test.Services
{
    public class OrderServiceTest
    {
        [Theory]
        [DefaultData]
        public void CreateOrder_Valid_Should_Work(
            CDTO.Order order,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            order.DeliveryAddress.UseToBilling = false;

            orderRepositoryMock
                .Setup(x => x.AddAsync(It.IsAny<ENT.Order>()))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () => await sut.CreateAsync(order);

            // assert
            act.Should().NotThrow();

            orderRepositoryMock.VerifyAll();
        }

        [Theory]
        [DefaultData]
        public void CreateOrder_Duplicated_Should_ThrowException(
            CDTO.Order newOrder,
            ENT.Order duplicatedOrder,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            newOrder.DeliveryAddress.UseToBilling = true;
            newOrder.BillingAddress = null;

            orderRepositoryMock
                .Setup(x => x.AddAsync(It.IsAny<ENT.Order>()))
                .Throws(new DuplicateEntityException<ENT.Order>(duplicatedOrder))
                .Verifiable();

            // act
            Func<Task> act = async () => await sut.CreateAsync(newOrder);

            // assert
            act.Should()
                .Throw<UserException>()
                .WithMessage($"Order with id '{newOrder.Cuid}' already exists in '{duplicatedOrder.CreatedOn}'");

            orderRepositoryMock.VerifyAll();
        }

        [Theory]
        [DefaultData]
        public void Receive_PaymentEvent_Should_Work(
            ENT.Order order,
            CDTO.PaymentEvent paymentEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            var eventsBefore = order.Events.Count;
            var paymentsBefore = order.Payments.Count;
            paymentEvent.OrderId = order.OrderId;
            paymentEvent.Amount = order.Amount / 2m;

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == paymentEvent.OrderId)))
                .ReturnsAsync(order);
            orderRepositoryMock
                .Setup(x => x.UpdateAsync(It.Is<ENT.Order>(o => o.OrderId == order.OrderId)))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () => await sut.CreatePaymentEventAsync(paymentEvent);

            // assert
            act.Should().NotThrow();
            orderRepositoryMock.VerifyAll();
            order.Events.Count.Should().Be(eventsBefore + 1);
            order.Payments.Count.Should().Be(paymentsBefore + 1);
            order.Events.Last().Type.Should().Be(ENT.EventTypeEnum.PaymentReceived);
            order.CurrentStatus.Value.Should().Be(ENT.Status.Options.PaymentReceived);
        }

        [Theory]
        [DefaultData]
        public void Receive_Complete_PaymentEvent_Should_Work(
            ENT.Order order,
            CDTO.PaymentEvent paymentEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            var eventsBefore = order.Events.Count;
            var paymentsBefore = order.Payments.Count;
            paymentEvent.OrderId = order.OrderId;
            paymentEvent.Amount = order.Amount;

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == paymentEvent.OrderId)))
                .ReturnsAsync(order);
            orderRepositoryMock
                .Setup(x => x.UpdateAsync(It.Is<ENT.Order>(o => o.OrderId == order.OrderId)))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () => await sut.CreatePaymentEventAsync(paymentEvent);

            // assert
            act.Should().NotThrow();
            orderRepositoryMock.VerifyAll();
            order.Events.Count.Should().Be(eventsBefore + 1);
            order.Events.Last().Type.Should().Be(ENT.EventTypeEnum.PaymentReceived);
            order.Payments.Count.Should().Be(paymentsBefore + 1);
            order.CurrentStatus.Value.Should().Be(ENT.Status.Options.Payed);
        }

        [Theory]
        [DefaultData]
        public void Receive_BillingEvent_Should_Work(
            ENT.Order order,
            CDTO.BillingEvent billingEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            var eventsBefore = order.Events.Count;
            billingEvent.OrderId = order.OrderId;
            order.CurrentStatus.Payed();

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == billingEvent.OrderId)))
                .ReturnsAsync(order);
            orderRepositoryMock
                .Setup(x => x.UpdateAsync(It.Is<ENT.Order>(o => o.OrderId == order.OrderId)))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () => await sut.CreateBillingEventAsync(billingEvent);

            // assert
            act.Should().NotThrow();
            orderRepositoryMock.VerifyAll();
            order.Events.Count.Should().Be(eventsBefore + 1);
            order.Events.Last().Type.Should().Be(ENT.EventTypeEnum.Billing);
            order.Billing.InvoiceNumber.Should().Be(billingEvent.InvoiceNumber);
        }

        [Theory]
        [DefaultData]
        public void Receive_NotPayed_BillingEvent_Should_ThrowException(
            ENT.Order order,
            CDTO.BillingEvent billingEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            var eventsBefore = order.Events.Count;
            billingEvent.OrderId = order.OrderId;

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == billingEvent.OrderId)))
                .ReturnsAsync(order);

            // act
            Func<Task> act = async () => await sut.CreateBillingEventAsync(billingEvent);

            // assert
            act.Should()
               .Throw<UserException>()
               .WithMessage($"Status change not allowed, '{order.CurrentStatus.Value.ToString()}' to '{ENT.Status.Options.Billing.ToString()}'");
            orderRepositoryMock.VerifyAll();
        }

        [Theory]
        [DefaultData]
        public void Receive_DeliveredEvent_Should_Work(
            ENT.Order order,
            CDTO.DeliveryEvent deliveryEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            var eventsBefore = order.Events.Count;
            deliveryEvent.OrderId = order.OrderId;
            order.CurrentStatus.Payed();

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == deliveryEvent.OrderId)))
                .ReturnsAsync(order);
            orderRepositoryMock
                .Setup(x => x.UpdateAsync(It.Is<ENT.Order>(o => o.OrderId == order.OrderId)))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () => await sut.CreateDeliveryEventAsync(deliveryEvent);

            // assert
            act.Should().NotThrow();
            orderRepositoryMock.VerifyAll();
            order.Events.Count.Should().Be(eventsBefore + 1);
            order.Events.Last().Type.Should().Be(ENT.EventTypeEnum.Delivery);
            order.Transitions.OrderBy(t => t.CreatedOn).Last().Value.Should().Be(ENT.Status.Options.Delivered);
            order.DeliveryAddress.DeliveryOn.Should().Be(deliveryEvent.DeliveryOn);
        }

        [Theory]
        [DefaultData]
        public void Receive_NotPayed__DeliveredEvent_Should_ThrowException(
            ENT.Order order,
            CDTO.DeliveryEvent deliveryEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            var eventsBefore = order.Events.Count;
            deliveryEvent.OrderId = order.OrderId;

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == deliveryEvent.OrderId)))
                .ReturnsAsync(order);

            // act
            Func<Task> act = async () => await sut.CreateDeliveryEventAsync(deliveryEvent);

            // assert
            act.Should()
               .Throw<UserException>()
               .WithMessage($"Status change not allowed, '{order.CurrentStatus.Value.ToString()}' to '{ENT.Status.Options.Delivered.ToString()}'");
            orderRepositoryMock.VerifyAll();
        }

        [Theory]
        [DefaultData]
        public void Terminate_Billed_Delivered_Order_Should_Work(
            ENT.Order order,
            CDTO.DeliveryEvent deliveryEvent,
            CDTO.BillingEvent billingEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut
            )
        {
            // arrange
            var eventsBefore = order.Events.Count;
            deliveryEvent.OrderId = order.OrderId;
            billingEvent.OrderId = order.OrderId;
            order.Payed();

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == order.OrderId)))
                .ReturnsAsync(order);
            orderRepositoryMock
                .Setup(x => x.UpdateAsync(It.Is<ENT.Order>(o => o.OrderId == order.OrderId)))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () =>
            {
                await sut.CreateDeliveryEventAsync(deliveryEvent);
                await sut.CreateBillingEventAsync(billingEvent);
            };

            // assert
            act.Should().NotThrow();
            orderRepositoryMock.VerifyAll();
            order.Events.Count.Should().Be(eventsBefore + 2);
            order.Transitions.Last().Value.Should().Be(ENT.Status.Options.Terminated);
            order.CurrentStatus.Value.Should().Be(ENT.Status.Options.Terminated);
        }

        [Theory]
        [DefaultData]
        public void CancellingOrder_Should_Work(
            ENT.Order order,
            CDTO.Event cancellingEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut
            )
        {
            // arrange
            var eventsBefore = order.Events.Count;
            cancellingEvent.OrderId = order.OrderId;

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == order.OrderId)))
                .ReturnsAsync(order);
            orderRepositoryMock
                .Setup(x => x.UpdateAsync(It.Is<ENT.Order>(o => o.OrderId == order.OrderId)))
                .Returns(Task.CompletedTask);

            // act
            Func<Task> act = async () => await sut.CreateCancelEventAsync(cancellingEvent);

            // assert
            act.Should().NotThrow();
            orderRepositoryMock.VerifyAll();
            order.Events.Count.Should().Be(eventsBefore + 1);
            order.Transitions.Last().Value.Should().Be(ENT.Status.Options.Cancelled);
            order.CurrentStatus.Value.Should().Be(ENT.Status.Options.Cancelled);
        }

        [Theory]
        [DefaultData]
        public void Receive_PaymentEvent_On_EndedOrder_Should_Throw_Exception(
            ENT.Order order,
            CDTO.DeliveryEvent deliveryEvent,
            CDTO.BillingEvent billingEvent,
            CDTO.PaymentEvent paymentEvent,
            [Frozen] Mock<IOrderRepository> orderRepositoryMock,
            OrderService sut)
        {
            // arrange
            deliveryEvent.OrderId = order.OrderId;
            billingEvent.OrderId = order.OrderId;
            order.Payed();
            paymentEvent.OrderId = order.OrderId;
            paymentEvent.Amount = order.Amount / 2m;

            orderRepositoryMock
                .Setup(x => x.GetAsync(It.Is<Guid>(guid => guid == paymentEvent.OrderId)))
                .ReturnsAsync(order);

            // act
            Func<Task> act = async () =>
            {
                await sut.CreateDeliveryEventAsync(deliveryEvent);
                await sut.CreateBillingEventAsync(billingEvent);
                await sut.CreatePaymentEventAsync(paymentEvent);
            };

            // assert
            act.Should()
               .Throw<UserException>()
               .WithMessage($"Order cannot be changed because of final state '{order.CurrentStatus.Value.ToString()}'");
            orderRepositoryMock.VerifyAll();
        }
    }
}
