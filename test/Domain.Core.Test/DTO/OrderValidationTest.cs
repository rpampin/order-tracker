﻿using FluentAssertions;
using FluentValidation.TestHelper;
using FVG.Utils.Testing.Fixtures.Attributes;
using Xunit;
using CDTO = Domain.Core.Commons.DTO;
using CVAL = Domain.Core.Commons.Validations;

namespace Domain.Core.Test.DTO
{
    public class OrderValidationTest
    {

        // TEST ORDER VALID
        [Theory(DisplayName = "1. Order validation when order is valid, should work")]
        [DefaultData]
        public void Validate_Order_Valid_Should_Work(
            CDTO.Order order,
            CVAL.OrderValidator sut)
        {
            order.DeliveryAddress.UseToBilling = false;

            // act
            var result = sut.Validate(order);

            // assert
            result.IsValid.Should().BeTrue();
        }

        // TEST ORDER INVALID (BILLING & DELIVERY)
        [Theory(DisplayName = "2. Order validation when order is invalid, should not work")]
        [DefaultData]
        public void ValidateOrder_InvalidBillingAddress_Should_Not_Work(
            CDTO.Order order,
            CVAL.OrderValidator sut)
        {
            order.DeliveryAddress.UseToBilling = true;

            // act
            var result = sut.Validate(order);

            // assert
            result.IsValid.Should().BeFalse();
            sut.ShouldHaveValidationErrorFor(o => o.BillingAddress, order);
        }
    }
}
